const http = require('http');
require('dotenv').config();

const { PORT, INTERVAL, TIMEOUT } = process.env;

const server = http.createServer((req, res) => {
  let interval = setInterval(() => {
    console.log(`${req.method} ${req.url}: ${new Date().toISOString()}`);
  }, INTERVAL);

  setTimeout(() => {
    clearInterval(interval);
    res.end(new Date().toISOString());
  }, TIMEOUT);
});

server.listen(PORT, () => {
  console.log(`Server running on port: ${PORT}`);
});
